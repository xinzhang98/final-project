cmake_minimum_required(VERSION 3.0.2)
project(fetch_grasp)

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  geometry_msgs
  image_geometry
  pcl_ros
  roscpp
  rospy
  sensor_msgs
  std_msgs
  fetch_msgs
  moveit_msgs
  image_transport
)

find_package(PCL REQUIRED)
find_package(Boost REQUIRED)
find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)

link_directories(
  ${catkin_LIBRARY_DIRS}
  ${Boost_LIBRARY_DIRS}
  ${PCL_LIBRARY_DIRS}
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

catkin_package(
 INCLUDE_DIRS include #${BOOST_INCLUDE_DIRS} ${EIGEN3_INCLUDE_DIRS}
  CATKIN_DEPENDS 
    fetch_msgs
    cv_bridge 
    geometry_msgs 
    image_geometry 
    pcl_ros 
    roscpp 
    rospy 
    sensor_msgs 
    std_msgs 
    fetch_msgs 
    image_transport
 LIBRARIES  
    fetch_grasp
 DEPENDS EIGEN3 Boost#system_lib 
)

link_directories(${PCL_LIBRARY_DIRS} ${Boost_LIBRARY_DIRS})

include_directories(
  include
  SYSTEM
  ${Boost_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
  ${Eigen3_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(fetch_grasp
  #src/perception_pipeline.cpp
  src/object_segmentation.cpp
  src/tools.cpp
  src/grasps_plan.cpp
)

target_link_libraries(fetch_grasp
  ${Boost_LIBRARIES}
  ${catkin_LIBRARIES}
  ${PCL_LIBRARIES}
)

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
# add_executable(fetch_grasp src/perception_pipeline.cpp src/object_segmentation.cpp src/tools.cpp)

add_executable(perception_pipeline src/perception_pipeline.cpp)
add_dependencies(perception_pipeline fetch_msgs_generate_messages_cpp)
target_link_libraries(perception_pipeline fetch_grasp 
${Boost_LIBRARIES}
${catkin_LIBRARIES}
${OpenCV_LIBS}
${PCL_LIBRARIES})
# add_dependencies(perception_pipeline ${perception_pipeline_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
# target_link_libraries(perception_pipeline
#    ${catkin_LIBRARIES} 
#    ${Boost_LIBRARIES}
#    ${PCL_LIBRARIES}
#    ${OpenCV_LIBS}
#  )

#  add_executable(object_segmentation src/object_segmentation.cpp)
#  add_dependencies(object_segmentation ${object_segmentation_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
# target_link_libraries(object_segmentation
#    ${catkin_LIBRARIES} 
#    ${Boost_LIBRARIES}
#    ${PCL_LIBRARIES}
#    ${OpenCV_LIBS}
#  )

#  add_executable(tools src/tools.cpp)
#  add_dependencies(tools ${tools_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
# target_link_libraries(tools
#    ${catkin_LIBRARIES} 
#    ${Boost_LIBRARIES}
#    ${PCL_LIBRARIES}
#    ${OpenCV_LIBS}
#  )

#  add_executable(object_segmentation.cpp)

#  install(
#   DIRECTORY include/
#   DESTINATION include
# )

# install(
#   TARGETS fetch_grasp perception_pipeline
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
# )
#  add_executable(object_segmentation src/object_segmentation.cpp)
#  add_dependencies(object_segmentation ${object_segmentation_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
#  target_link_libraries(object_segmentation
#     ${catkin_LIBRARIES} 
#     ${PCL_LIBRARIES}
#     ${OpenCV_LIBS}
#   )

#   add_executable(tools src/tools.cpp)
#   add_dependencies(tools ${tools_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
#   target_link_libraries(tools
#      ${catkin_LIBRARIES} 
#      ${PCL_LIBRARIES}
#      # ${OpenCV_LIBS}
#    )

# ### Test
# if (CATKIN_ENABLE_TESTING)
# add_subdirectory(test)
# endif()

### Install
install(
  DIRECTORY include/
  DESTINATION include
)

install(
  TARGETS fetch_grasp perception_pipeline
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)