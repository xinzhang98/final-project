#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <tf/transform_listener.h>

// #include <simple_grasping/object_support_segmentation.h>
// #include <simple_grasping/shape_grasp_planner.h>
#include <fetch_grasp/object_segmentation.h>
// #include <fetch_grasp/tools.h>
#include <fetch_msgs/DetectGraspableObjectAction.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>
#include <pcl_conversions/pcl_conversions.h>
#include <fetch_grasp/grasps_plan.h>

namespace fetch_grasp {

  class Perception {
    typedef actionlib::SimpleActionServer<fetch_msgs::DetectGraspableObjectAction> server_t;

    public:
      Perception(ros::NodeHandle &n): nh_(n) //: segmentation_(nh_)//: nh_(n)
      {
        base_frame_ = "base_link";
        camera_frame = "head_camera_rgb_optical_frame";
        image_topic = "/head_camera/rgb/image_raw";
        cloud_topic = "/head_camera/depth_registered/points";
        // ObjectSegmentation test(n);
        segmentation_.reset(new ObjectSegmentation(nh_));
        planner_.reset(new GraspsPlanner(nh_));
        // ObjectSegmentation seg(nh_);
        // segmentation_ = new ObjectSegmentation(nh_);
        // ObjectSegmentation test(nh_);
        server_.reset(new server_t(nh_, "find_objects",
                               boost::bind(&Perception::executeCallback, this, _1),
                               false));
        object_cloud_pub_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("object_cloud", 1);
        support_cloud_pub_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("support_cloud", 1);
        range_filter_.setFilterFieldName("z");
        range_filter_.setFilterLimits(0.5, 2.5);
        cloud_sub_ = nh_.subscribe< pcl::PointCloud<pcl::PointXYZRGB> >(cloud_topic, 1, &Perception::cloudCb, this);
        server_->start();
      }

    private:
      ros::NodeHandle nh_;
      bool find_objects_;
      tf::TransformListener listener_;
      std::string base_frame_; std::string camera_frame;
      std::string image_topic; std::string cloud_topic;
      std::vector<fetch_msgs::Object> objects_;
      std::vector<fetch_msgs::Object> supports_;
      ros::Subscriber cloud_sub_;
      ros::Publisher object_cloud_pub_;
      ros::Publisher support_cloud_pub_;
      boost::shared_ptr<ObjectSegmentation> segmentation_;
      boost::shared_ptr<server_t> server_;
      boost::shared_ptr<GraspsPlanner> planner_;
      // grasp_planner = GraspsPlanner();
      pcl::PassThrough<pcl::PointXYZRGB> range_filter_;
      // std::vector<moveit_msgs::Grasp> grasps_;

      void cloudCb(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& cloud) {
        if (!find_objects_){return;}
        // tf::StampedTransform transform;
        // ros::Duration timeout(1);
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);
        range_filter_.setInputCloud(cloud);
        range_filter_.filter(*cloud_filtered);
        // tf_listener_.waitForTransform(base_frame_, source_frame, ros::Time::now(), ros::Duration(1.0));
        // tf_listener_.lookupTransform(frame, source_frame, ros::Time(0), transform);

        // tf::TransformListener _tf_listener_;
        // std::string frame = "head_camera_rgb_optical_frame";
        // std::string source_frame = "base_link";
        // tf::StampedTransform transform;
        // ros::Duration timeout(1);
        // listener_.waitForTransform(source_frame, frame, ros::Time::now(), ros::Duration(1.0));
        // listener_.lookupTransform(source_frame, frame, ros::Time(0), transform);
        // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_transformed(new pcl::PointCloud<pcl::PointXYZRGB>);
        // pcl_ros::transformPointCloud(source_frame, cloud_filtered, *cloud_transformed, listener_);

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_transformed(new pcl::PointCloud<pcl::PointXYZRGB>);
        if (!pcl_ros::transformPointCloud(base_frame_, *cloud_filtered, *cloud_transformed, listener_)) {return;}

        objects_.clear();
        supports_.clear();
        pcl::PointCloud<pcl::PointXYZRGB> object_cloud;
        pcl::PointCloud<pcl::PointXYZRGB> support_cloud;
        object_cloud.header.frame_id = cloud_transformed->header.frame_id;
        support_cloud.header.frame_id = cloud_transformed->header.frame_id;
        segmentation_->segment(cloud_transformed, objects_, supports_, object_cloud, support_cloud);
        object_cloud_pub_.publish(object_cloud);
        support_cloud_pub_.publish(support_cloud);
        find_objects_ = false;
      }

      void executeCallback(const fetch_msgs::DetectGraspableObjectGoalConstPtr& goal) {
        fetch_msgs::DetectGraspableObjectResult result;
        find_objects_ = true;
        ros::Time t = ros::Time::now();
        while (find_objects_ == true)
        {
          ros::Duration(1/50.0).sleep();
          if (ros::Time::now() - t > ros::Duration(10.0))
          {
            find_objects_ = false;
            server_->setAborted(result, "Failed to get camera data in alloted time.");
            // ROS_ERROR("Failed to get camera data in alloted time.");
            return;
          }
        }

        // Set object results
        for (size_t i = 0; i < objects_.size(); ++i)
        {
          fetch_msgs::GraspableObject g;
          g.object = objects_[i];
          if (goal->plan_grasps)
          {
            // Plan grasps for object
            // planner_.reset(new GraspsPlanner(objects_[i], g.grasps));
            planner_->plan(objects_[i], g.grasps);
            // grasp
          }
          result.objects.push_back(g);
        }
        // Set support surfaces
        result.support_surfaces = supports_;

        server_->setSucceeded(result, "Succeeded.");
      }
  };
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "perception_pipeline_node");
  ros::NodeHandle n;
  fetch_grasp::Perception pipeline(n);
  // fetch_grasp::ObjectSegmentation test(n);
  ros::spin();
  return 0;
}