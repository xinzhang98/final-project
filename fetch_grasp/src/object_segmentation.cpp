#include <fetch_grasp/object_segmentation.h>
#include <eigen3/Eigen/Eigen>
#include <boost/lexical_cast.hpp>
#include <fetch_grasp/tools.h>
#include <pcl/io/io.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl_conversions/pcl_conversions.h>

namespace fetch_grasp {

  ObjectSegmentation::ObjectSegmentation(ros::NodeHandle &nh)
  {
    // image_transport::ImageTransport it_(nh);
    // pointsFromPc.clear();
    // points2Img.clear();
    
	  double cluster_tolerance = 0.01;
	  int cluster_min_size = 50;
	  double leaf_size = 0.005;
	  double lower_limit = 0.0;
	  double upper_limit = 1.8;
	  string field = "z";
	  vg.setLeafSize(leaf_size, leaf_size, leaf_size);
	  vg.setFilterFieldName(field);
	  vg.setFilterLimits(lower_limit, upper_limit);
	  seg.setOptimizeCoefficients(true);
	  seg.setModelType(pcl::SACMODEL_PLANE);
	  seg.setMaxIterations(100);
	  seg.setDistanceThreshold(cluster_tolerance);
	  extract_cluster.setClusterTolerance(cluster_tolerance);
	  extract_cluster.setMinClusterSize(cluster_min_size);
    // it_.subscribeCamera("/head_camera/rgb/camera_info", 1, &ObjectSegmentation::imageCb, this);
    sub_ = nh.subscribe<sensor_msgs::CameraInfo>("/head_camera/rgb/camera_info",1, &ObjectSegmentation::subCb, this);
  }

  void ObjectSegmentation::subCb(const sensor_msgs::CameraInfoConstPtr& info_msg) {
    cam_model_.fromCameraInfo(info_msg);
  }

  // void ObjectSegmentation::imageCb(const sensor_msgs::ImageConstPtr& image_msg,
  //                                  const sensor_msgs::CameraInfoConstPtr& info_msg) {
  //   cam_model_.fromCameraInfo(info_msg);
  // }

  void ObjectSegmentation::pc2img(const pcl::PointCloud<pcl::PointXYZRGB> &data) {
    // msg.data.push_back(0.124);
    pointsFromPc.clear();
    points2Img.clear();
    ROS_INFO("start to pc2img");
    std::string frame = "head_camera_rgb_optical_frame";
    std::string source_frame = "base_link";
    tf::StampedTransform transform;
    ros::Duration timeout(1);
    tf_listener_.waitForTransform(frame, source_frame, ros::Time::now(), ros::Duration(1.0));
    tf_listener_.lookupTransform(frame, source_frame, ros::Time(0), transform);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr out(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl_ros::transformPointCloud(frame, data, *out, tf_listener_);
    BOOST_FOREACH (const pcl::PointXYZRGB& pt, out->points){
      cv::Point3f new_pt(pt.x, pt.y, pt.z);
      pointsFromPc.push_back(new_pt);
    }

    tf::StampedTransform transform_;
    try {
      // ros::Time acquisition_time = info_msg->header.stamp;
      ros::Duration timeout(1.0);
      tf_listener_.waitForTransform(cam_model_.tfFrame(), source_frame,
                                    ros::Time(0), timeout);
      tf_listener_.lookupTransform(cam_model_.tfFrame(), source_frame,
                                   ros::Time(0), transform_);
    }
    catch (tf::TransformException& ex) {
      ROS_WARN("TF exception:\n%s", ex.what());
      return;
    }

    if(pointsFromPc.size()!=0) {
      for(size_t i = 0; i < pointsFromPc.size(); ++i) {
        cv::Point3f pt_cv(pointsFromPc[i].x, pointsFromPc[i].y, pointsFromPc[i].z);
        cv::Point2f new_2dPt = cam_model_.project3dToPixel(pt_cv);
        if(new_2dPt.x > 0 && new_2dPt.x < 640) {
          if(new_2dPt.y > 0 && new_2dPt.y < 480) {
            points2Img.push_back(new_2dPt);
          }
        }
      }
      ROS_INFO("2d point size is %zd", points2Img.size());
    }
  }

  void ObjectSegmentation::segment(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud,
						           vector<fetch_msgs::Object> &objects,
						           vector<fetch_msgs::Object> &planes,
						           pcl::PointCloud<pcl::PointXYZRGB> &objects_cloud,
						           pcl::PointCloud<pcl::PointXYZRGB> &planes_cloud)
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);
    vg.setInputCloud(cloud);
    vg.filter(*cloud_filtered);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr non_horizontal_planes(new pcl::PointCloud<pcl::PointXYZRGB>);
    vector<pcl::ModelCoefficients::Ptr> plane_coefficients;
    int thresh = cloud_filtered->points.size()/8;
    while(cloud_filtered->points.size() > 500)  {
      pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
      pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
      seg.setInputCloud(cloud_filtered);
      seg.segment(*inliers, *coefficients);
      if (inliers->indices.size() < (size_t) thresh) {
        ROS_DEBUG("No more planes to remove.");
        break;
      }
      pcl::PointCloud<pcl::PointXYZRGB> plane;
      pcl::ExtractIndices<pcl::PointXYZRGB> extract;
      extract.setInputCloud(cloud_filtered);
      extract.setIndices(inliers);
      extract.setNegative(false);
      extract.filter(plane);
      Eigen::Vector3f normal(coefficients->values[0], 
                             coefficients->values[1], 
                             coefficients->values[2]);
      float angle = acos(Eigen::Vector3f::UnitZ().dot(normal));
      if(angle < 0.5) {
        fetch_msgs::Object obj;
        pcl::toROSMsg(plane, obj.point_cluster);
        obj.name = string("Plane_") + boost::lexical_cast<string>(planes.size());
        shape_msgs::SolidPrimitive box;
        geometry_msgs::Pose pose;
        extractPlaneBoundingBox(plane, box, pose);
        obj.primitives.push_back(box);
        obj.primitive_poses.push_back(pose);
        for(int i = 0; i < 4; ++i) {
          obj.surface.coef[i] = coefficients->values[i];
        }
        obj.header.stamp = ros::Time::now();
        obj.header.frame_id = cloud->header.frame_id;
        planes.push_back(obj);
        planes_cloud += plane;
        plane_coefficients.push_back(coefficients);
      } else {
        *non_horizontal_planes += plane;
      }
      extract.setNegative(true);
      extract.filter(*cloud_filtered);
    }
    *cloud_filtered += *non_horizontal_planes;

    vector<pcl::PointIndices> clusters;
    extract_cluster.setInputCloud(cloud_filtered);
    extract_cluster.extract(clusters);
    extract_indices.setInputCloud(cloud_filtered);
    for(size_t i = 0; i < clusters.size(); ++i )  {
      pcl::PointCloud<pcl::PointXYZRGB> new_cloud;
      extract_indices.setIndices(pcl::PointIndicesPtr(new pcl::PointIndices(clusters[i])));
      extract_indices.filter(new_cloud);
      Eigen::Vector4f centroid;
      pcl::compute3DCentroid(new_cloud, centroid);
      
      int support_plane_index = -1;
      double support_plane_distance = 1000.0;
      for (int p = 0; p < plane_coefficients.size(); ++p)
      {
        double distance = distancePointToPlane(centroid, plane_coefficients[p]);
        if (distance > 0.0 && distance < support_plane_distance) {
          support_plane_distance = distance;
          support_plane_index = p;
        }
      }
      if (support_plane_index == -1) {continue;}
      fetch_msgs::Object obj;
      obj.support_surface = string("surface") + boost::lexical_cast<string>(support_plane_index);
      shape_msgs::SolidPrimitive box;
      geometry_msgs::Pose pose;
      pcl::toROSMsg(new_cloud, obj.point_cluster);

      // Starting computing features for regarding object
      // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      // Convert 3D coordinates into 2D coordinates (Geometry->Image)
      // Input := Cluster of type Point Cloud (XYZRGB)
      // Output := vector<cv2::Point> (Type Float32 required for OpenCv modules)
      std_msgs::Float32MultiArray img_point;
      pc2img(new_cloud);
      img_point.layout.dim.push_back(std_msgs::MultiArrayDimension());
      img_point.layout.dim[0].label = "img_pixel";
      img_point.layout.dim[0].size = points2Img.size();
      img_point.layout.dim[0].stride = new_cloud.points.size();
      img_point.data.clear();
      for(int num = 0; num < points2Img.size(); ++num) {
        img_point.data.push_back(points2Img.at(num).x);
        img_point.data.push_back(points2Img.at(num).y);
      }
      obj.project_points = img_point;


      // Compute FPFH (PCL descriptor) with complexity of O(nk^2) which is faster than PFH algorithm
      // Input := Cluster of type Point Cloud (XYZRGB) PS: RGB values are not necessary
      // Ouput := Float32[]
      std_msgs::Float32MultiArray fpfh_result;
      // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPTR(new pcl::PointCloud<pcl::PointXYZRGB>); // Due to the 
      // *cloudPTR = createPointCloud(new_cloud);
      // // compute_fpfh_n_project_points(new_cloud, output);
      pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
      pcl::PointCloud<pcl::FPFHSignature33>::Ptr descriptors(new pcl::PointCloud<pcl::FPFHSignature33>);
      pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> normalEstimation;
      normalEstimation.setInputCloud(new_cloud.makeShared());
	    normalEstimation.setRadiusSearch(0.03);
      pcl::search::KdTree<pcl::PointXYZRGB>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZRGB>);
      normalEstimation.setSearchMethod(kdtree);
	    normalEstimation.compute(*normals);
      pcl::FPFHEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::FPFHSignature33> fpfh;
      fpfh.setInputCloud(new_cloud.makeShared());
	    fpfh.setInputNormals(normals);
	    fpfh.setSearchMethod(kdtree);
      fpfh.setRadiusSearch(0.05);
	    fpfh.compute(*descriptors);
      fpfh_result.data.clear();
      fpfh_result.layout.dim.push_back(std_msgs::MultiArrayDimension());
      fpfh_result.layout.dim[0].label = "fpfh_descriptor";
      fpfh_result.layout.dim[0].size = descriptors->size();
      fpfh_result.layout.dim[0].stride = new_cloud.points.size();
      int nV = descriptors->size(), nDim = 33;
      for(int i = 0; i < nV; ++i) {
        for(int j = 0; j < nDim; ++j) {
          fpfh_result.data.push_back(descriptors->points[i].histogram[j]);
        }
      }
      obj.fpfh_descriptor = fpfh_result;

      /* Results from two sections above are as parameters passed into a Python module */
      // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      pcl::PointCloud<pcl::PointXYZRGB> projected_cloud;
      extractObjectBoundingBox(new_cloud, plane_coefficients[support_plane_index], projected_cloud, box, pose);
      // pcl::toROSMsg(projected_cloud, obj.point_cluster);
      
      obj.primitives.push_back(box);
      obj.primitive_poses.push_back(pose);
      // add stamp and frame
      obj.header.stamp = ros::Time::now();
      obj.header.frame_id = cloud->header.frame_id;
      objects.push_back(obj);
      objects_cloud += new_cloud;
    }
    return;
  }

}

// int main(int argc, char** argv) {
//   ros::init(argc, argv, "test_node");
//   ros::NodeHandle n;
//   fetch_grasp::ObjectSegmentation test(n);
//   // fetch_grasp::ObjectSegmentation test(n);
//   ros::spin();
//   return 0;
// }
