#!/usr/bin/env python

import copy
import actionlib
import pickle
import rospy
import numpy as np
import cv2
import copy
import time
import math
import sklearn
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from math import sin, cos
from moveit_python import (MoveGroupInterface,
                           PlanningSceneInterface,
                           PickPlaceInterface)
from moveit_python.geometry import rotate_pose_msg_by_euler_angles
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from control_msgs.msg import PointHeadAction, PointHeadGoal
from fetch_msgs.msg import DetectGraspableObjectAction, DetectGraspableObjectGoal
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion, Twist
from nav_msgs.msg import Odometry
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from moveit_msgs.msg import PlaceLocation, MoveItErrorCodes
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import Image
from std_msgs.msg import String, Header
from cv_bridge import CvBridge, CvBridgeError
from tf.transformations import quaternion_about_axis
import pickle
from visualization_msgs.msg import Marker
import sensor_msgs.point_cloud2 as pc2

"""
  Reference: https://docs.fetchrobotics.com/manipulation.html#running-the-pick-and-place-demo
  This is what I got from the official Fetch website and modified it to fit my project needs
  peception_client.py is the client that re
  
  The perception_client is mainly a client to connect to the perception pipeline to get the results after
  computing the point cloud, where the perception pipeline has already computed the grasps and
  approximate shape of the object.
"""


class TorsoClient(object):
    def __init__(self):
        """Call the torso_controller provided by Fetch
           Raising torso to a position
        """
        self.client = actionlib.SimpleActionClient("torso_controller/follow_joint_trajectory",
                                                   FollowJointTrajectoryAction)
        rospy.loginfo("Connecting torso_controller...")
        self.client.wait_for_server()
        self.joint_names = ["torso_lift_joint"]
    
    def raise_up(self, positions, duration=5.0):
        """Raise Torso to identified position

        Args:
            positions ([list]): The maximum range that Fetch can rasied up
            duration (float, optional): Sleep a few seconds after reach the position. Defaults to 5.0.

        Returns:
            None
        """
        if len(self.joint_names) != len(positions):
            print("Invalid trajectory position")
            return False
        trajectory = JointTrajectory()
        trajectory.joint_names = self.joint_names
        trajectory.points.append(JointTrajectoryPoint())
        trajectory.points[0].positions = positions
        trajectory.points[0].velocities = [0.0 for _ in positions]
        trajectory.points[0].accelerations = [0.0 for _ in positions]
        trajectory.points[0].time_from_start = rospy.Duration(duration)
        follow_goal = FollowJointTrajectoryGoal()
        follow_goal.trajectory = trajectory

        self.client.send_goal(follow_goal)
        self.client.wait_for_result()

class HeadClient(object):
    def __init__(self):
        """Call head_controller, and let Fetch focus on a close range
        """
        self.client = actionlib.SimpleActionClient("head_controller/point_head", PointHeadAction)
        rospy.loginfo("Connecting head_controller...")
        self.client.wait_for_server()

    def look(self, x, y, z, frame, duration=1.0):
        """[summary]

        Args:
            x ([type]): [description]
            y ([type]): [description]
            z ([type]): [description]
            frame ([type]): [description]
            duration (float, optional): [description]. Defaults to 1.0.
        """
        goal = PointHeadGoal()
        goal.target.header.stamp = rospy.Time.now()
        goal.target.header.frame_id = frame
        goal.target.point.x = x
        goal.target.point.y = y
        goal.target.point.z = z
        goal.min_duration = rospy.Duration(duration)
        self.client.send_goal(goal)
        self.client.wait_for_result()

class BaseClient(object):
    def __init__(self):
        """Move the base to a identified position
           Should listen the /odom topic from ROS
        """
        self.vel_pub = rospy.Publisher('/base_controller/command', Twist, queue_size=1)
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.odom_callback)
        self.actual_pose = Pose()
    
    def odom_callback(self,msg):
        """Listening /odom topic to reach the position

        Args:
            msg ([type]): The data from the topic
        """
        self.actual_pose = msg.pose.pose

    def move_to(self, twist_object):
        """ Call this to move base
        Args:
            twist_object ([type]): Specify the position
        """
        self.vel_pub.publish(twist_object)
    
    def stop(self):
        """Stop when reach position
        """
        vel_msg = Twist()
        self.move_to(vel_msg)
        
    def move(self, speed, target_x, tor=0.1):
        """ Call this to move the position

        Args:
            speed ([type]): Define the maximum spped
            target_x ([type]): We move along with x-axis
            tor (float, optional): Defaults to 0.1.
        """
        rate = rospy.Rate(5)
        vel_msg = Twist()
        
        actual_x = self.actual_pose.position.x
        vel_msg.linear.x = np.sign(target_x - actual_x)*speed
        
        in_place = False
        while not in_place:
            self.move_to(vel_msg)
            x_actual = self.actual_pose.position.x
            if  abs(target_x - x_actual) <= tor:
                break # Reached position
            rate.sleep()
        self.stop()

class PerceptionClient(object):
    def __init__(self):
      """ Initilize the core part:
          1. Loading the predictor file
          2. Connecting to the perception pipeline
          3. Prepare the MoveIt! module
          4. Set the interest lists
          5. Connect the ROS topics
      """
      # Test Part
      rospy.loginfo("Opening Predictor File")
      self.model = pickle.load(open('/home/shin/fetch_ws/src/fetch_grasp/scripts/model.sav', 'rb'))
      self.clf = self.model['classifier']
      self.encoder = LabelEncoder()
      self.encoder.classes_ = self.model['classes']
      self.scaler = self.model['scaler']
      rospy.loginfo("Loaded, Prepare to object perception pipeline")
        
      self.scene = PlanningSceneInterface("base_link")
      self.pickplace = PickPlaceInterface("arm", "gripper", verbose=True)
      self.move_group = MoveGroupInterface("arm", "base_link")
      
      find_topic = "/find_objects"
      rospy.loginfo("Connecting %s..." % find_topic)
      self.find_client = actionlib.SimpleActionClient(find_topic, DetectGraspableObjectAction)
      self.find_client.wait_for_server()
      self.sub_ = rospy.Subscriber("/head_camera/rgb/image_raw", Image, self.img_cb)
      self.marker_pub  = rospy.Publisher('/find_objects/marker', Marker, queue_size=1)
      
      self.bridge = CvBridge()
      self.point2d = []
      self.cv_image = None
      
      self.blue_interest_list = ['blue_cube']
      self.green_interest_list = ['green_cube']
      self.target_obj = []

    def make_label(self, text, _id, position, color=[1.0,1.0,1.0]):
        """ Helper function to label objects in MoveIt

        Args:
            text ([string]): Text string to be displayed.
            _id ([integer]): Integer identifying the label
            position ([list]): 3D position relating to the object
            color (list, optional): (R, G, B). Defaults to [1.0,1.0,1.0].

        Returns:
            [Marker]: A text view marker which can be published to RViz
        """
        marker = Marker()
        marker.header.frame_id = 'base_link'
        marker.id = _id
        marker.type = marker.TEXT_VIEW_FACING
        marker.text = text
        marker.action = marker.ADD
        marker.scale.x = 0.05
        marker.scale.y = 0.05
        marker.scale.z = 0.05
        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        marker.lifetime = rospy.Duration(5)
        marker.pose.orientation.w = 1.0
        marker.pose.position.x = float (position[0])
        marker.pose.position.y = float (position[1])
        marker.pose.position.z = float (position[2])
        return marker

    def compute_colour_histogram(self, image):
      """ Reference: OpenCV

      Args:
          image ([sensor_msgs/Image]): The cropped image

      Returns:
          [Colour historgram (list)]: Its a large number
      """
      bgr_planes = cv2.split(image)
      histSize = 256
      histRange = (0, 256)
      accumulate = False
      b_hist = cv2.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)
      g_hist = cv2.calcHist(bgr_planes, [1], None, [histSize], histRange, accumulate=accumulate)
      r_hist = cv2.calcHist(bgr_planes, [2], None, [histSize], histRange, accumulate=accumulate)
      hist_w = 512
      hist_h = 400
      bin_w = int(round(hist_w/histSize))
      histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)
      cv2.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      cv2.normalize(g_hist, g_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      cv2.normalize(r_hist, r_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      for i in range(1, histSize):
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(b_hist[i-1]) ),
              ( bin_w*(i), hist_h - int(b_hist[i]) ),
              ( 255, 0, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(g_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(g_hist[i]) ),
                ( 0, 255, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(r_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(r_hist[i]) ),
                ( 0, 0, 255), thickness=2)
      return histImage
    
    def img_cb(self, data):
      """ Listening the head camera, and get the RGB image

      Args:
          data ([sensor_msgs/Image]): RGB image
      """
      try:
        self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      except CvBridgeError as e:
        print(e)

    def scanScene(self):
        """ Scan the environemnt and get the infomation:
            1. Get the results from the perception pipeline
            2. Crop the image based on the pixel values, and compute the histogram
            3. Process the results -> Combine the FPFH descriptor with colour histogram
            4. Using the predictor to identify the objects in the scene, and label them in the scene
            5. Map objects to the target boxes, and add them in the MoveIt scene as a solid box
        """
        # Send goal to perception pipeline
        goal = DetectGraspableObjectGoal()
        goal.plan_grasps = True
        self.find_client.send_goal(goal)
        self.find_client.wait_for_result(rospy.Duration(5.0))
        find_result = self.find_client.get_result()

        # Remove last markers from the scene
        for name in self.scene.getKnownCollisionObjects():
            self.scene.removeCollisionObject(name, False)
        for name in self.scene.getKnownAttachedObjects():
            self.scene.removeAttachedObject(name, False)
        self.scene.waitForSync()
        
        img_threshold = 3
        objects = list()
        target_location = list()
        centroids = []
        idx = 0
        index = 0
        for obj in find_result.objects:
          # Get FPFH Descriptor
          fpfh_arr = np.array(obj.object.fpfh_descriptor.data, np.float32)
          fpfh = fpfh_arr.reshape(obj.object.fpfh_descriptor.layout.dim[0].size, 33)
          fpfh = fpfh[:200]
          list_ = np.array(obj.object.project_points.data, np.float32)
          
          # Compute min_x, max_x, min_y, max_y
          x,y,w,h = cv2.boundingRect(np.reshape(list_, (-1,2)))
          if w < img_threshold or h < img_threshold:
            continue
          
          # Crop out ROI from the image
          image = self.cv_image[y:y+h, x:x+w]
          histImage = self.compute_colour_histogram(image)
          feature = np.append(histImage[20:390], fpfh).astype(np.float64)
          prediction = self.clf.predict(self.scaler.transform(feature.reshape(1,-1)))
          
          # Predict Results with these features
          label_ = self.encoder.inverse_transform(prediction)[0]
          
          # Modify the demo_cube's name
          if 'demo' in label_:
            label_ = 'blue_cube'
            
          # # Debug
          point_array = pc2.read_points(obj.object.point_cluster, skip_nans=True)
          _pointcloud = []
          for p in point_array:
             _pointcloud.append(p[0])
             _pointcloud.append(p[1])
             _pointcloud.append(p[2])
          _pointcloud = np.array(_pointcloud)
          _pointcloud = np.reshape(_pointcloud, (-1,3))
          centroids.append(np.mean(_pointcloud, axis=0)[:3])
          centroids[idx][2] += .4
          self.marker_pub.publish(self.make_label(label_, idx, centroids[idx]))
          idx += 1
          
          # Get the target, assuming there is only one target on the table
          obj.object.name = label_
          if 'blue_target' == label_:
            self.blue_target = obj.object
            self.scene.addSolidPrimitive(obj.object.name,
                                         obj.object.primitives[0],
                                         obj.object.primitive_poses[0],
                                         use_service=False)
            continue
          
          # Get the green target, assuming there is only one green target on the table
          if 'green_target' == label_:
            self.green_target = obj.object
            self.scene.addSolidPrimitive(obj.object.name,
                                         obj.object.primitives[0],
                                         obj.object.primitive_poses[0],
                                         use_service=False)
            continue
          # objects.append([label_,obj])
          objects.append(obj)
        
        # Pair graspable objects with those targets
        obj_n_target = list()
        _index = 0
        for item in objects:
          if item.object.name in self.blue_interest_list:
            item.object.name += str(_index)
            obj_n_target.append([item, self.blue_target])
            _index += 1
          elif item.object.name in self.green_interest_list:
            item.object.name += str(_index)
            obj_n_target.append([item, self.green_target])
            _index += 1
          else:
            item.object.name = 'uninterested' + str(_index)
            _index += 1
          self.scene.addSolidPrimitive(item.object.name,
                             item.object.primitives[0],
                             item.object.primitive_poses[0],
                             use_service=False)

          
          
        
         # Add the surfaces as a SOLIDBOX, slightly extend the surface area
        for obj in find_result.support_surfaces:
          height = obj.primitive_poses[0].position.z
          obj.primitives[0].dimensions = [obj.primitives[0].dimensions[0],
                                          1.0,
                                          obj.primitives[0].dimensions[2] + height]
          obj.primitive_poses[0].position.z += -height/2.0
          # add to scene
          self.scene.addSolidPrimitive(obj.name,
                                       obj.primitives[0],
                                       obj.primitive_poses[0],
                                       use_service=False)

        self.scene.waitForSync()
        self.surfaces = find_result.support_surfaces
        # Make the variable gobally
        # Successful get the target
        self.objectPairs = [object for object in obj_n_target]
        
    def getGraspableObject(self):
        """ Find the objects with limitations

        Returns:
            [list]: The graspable object and the related target location
        """
        graspable = None
        for l in self.objectPairs:
            print(l[0].object.name)
            if l[1] is None:
              continue
            # Exclude the non-graspable object
            if len(l[0].grasps) < 1:
                continue
            # check size
            if l[0].object.primitives[0].dimensions[0] < 0.03 or \
               l[0].object.primitives[0].dimensions[0] > 0.25 or \
               l[0].object.primitives[0].dimensions[0] < 0.03 or \
               l[0].object.primitives[0].dimensions[0] > 0.25 or \
               l[0].object.primitives[0].dimensions[0] < 0.03 or \
               l[0].object.primitives[0].dimensions[0] > 0.25:
                continue
            print(l[0].object.primitives, l[0].object.primitive_poses)
            return l, True
        # nothing detected
        return None, None

    def getSupportSurface(self, name):
      """ Debug Tools

      Args:
          name ([string]): The name of the table in the scene

      Returns:
          [string]: Name
      """
      if surface.name == name:
          return surface
      return None

    def getTargetPlace(self):
        """ Debugging tool

        Returns:
            [Three lists]: Used for debugging
        """
        return self.target_obj, self.blue_interest_list, self.green_interest_list
    
    def pick(self, _object, grasps):
        """ Pick function

        Args:
            _object ([fetch_msgs/Object]): The graspable object ready to be picked
            grasps ([MoveIt/Grasp]): [description]

        Returns:
            [bool]: True -> Ready for placing process
                    False -> restart pick process
        """
        success, _pick_result = self.pickplace.pick_with_retry(_object.name,
                                                              grasps,
                                                              support_name="Plane_0",
                                                              scene=self.scene)
        self._pick_result = _pick_result
        return success
    
    def place(self, _object, pose_stamped):
        """ Place function

        Args:
            _object ([fetch_msgs/GraspableObject]): Already picked, try to place with a target place
            pose_stamped ([geometry_msg/Pose]): Identify a place location

        Returns:
            [bool]: True -> Placed in the target box
                    Fail -> Restart place process
        """
        places = list()
        
        place2location = PlaceLocation()
        place2location.place_pose.pose = pose_stamped.pose
        place2location.place_pose.header.frame_id = pose_stamped.header.frame_id
        # Debug
        # print(pose_stamped.pose)
        place2location.post_place_posture = self._pick_result.grasp.pre_grasp_posture
        place2location.pre_place_approach = self._pick_result.grasp.pre_grasp_approach
        place2location.post_place_retreat = self._pick_result.grasp.post_grasp_retreat

        # Debug
        # print(place2location)
        places.append(copy.deepcopy(place2location))
        
        m = 16  # number of possible place poses
        pi = 3.141592653589
        for i in range(0, m-1):
          place2location.place_pose.pose.position.z +=0.01
          place2location.place_pose.pose = rotate_pose_msg_by_euler_angles(
              place2location.place_pose.pose, 2 * pi / m, 2 * pi / m, 2 * pi / m)
          places.append(copy.deepcopy(place2location))
          
        success, place_result = self.pickplace.place_with_retry(_object.name,
                                                                places,
                                                                scene=self.scene)
        return success
      
    def tuck(self):
        joints = ["shoulder_pan_joint", "shoulder_lift_joint", "upperarm_roll_joint",
                  "elbow_flex_joint", "forearm_roll_joint", "wrist_flex_joint", "wrist_roll_joint"]
        pose = [1.32, 1.40, -0.2, 1.72, 0.0, 1.66, 0.0]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return

    def stow(self):
        joints = ["shoulder_pan_joint", "shoulder_lift_joint", "upperarm_roll_joint",
                  "elbow_flex_joint", "forearm_roll_joint", "wrist_flex_joint", "wrist_roll_joint"]
        pose = [1.32, 0.7, 0.0, -2.0, 0.0, -0.57, 0.0]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return

    def intermediate_stow(self):
        joints = ["shoulder_pan_joint", "shoulder_lift_joint", "upperarm_roll_joint",
                  "elbow_flex_joint", "forearm_roll_joint", "wrist_flex_joint", "wrist_roll_joint"]
        pose = [0.7, -0.3, 0.0, -0.3, 0.0, -0.57, 0.0]
        while not rospy.is_shutdown():
            result = self.move_group.moveToJointPosition(joints, pose, 0.02)
            if result.error_code.val == MoveItErrorCodes.SUCCESS:
                return

if __name__ == "__main__":
    rospy.init_node("perception_client_node")
    while not rospy.Time.now():
        pass
    
    attemps = 0
    # Preprocessing
    # Setup clients
    torso_client = TorsoClient()
    base_client = BaseClient()
    head_client = HeadClient()
    rospy.loginfo("Starting preparation process...")
    rospy.loginfo("Moving base...")
    base_client.move(0.29, 1.1, 0.1)
    rospy.loginfo("Moving torso...")
    torso_client.raise_up([0.4, ])
    rospy.sleep(0.5)
    head_client.look(0.9, 0.0, 0.5, "base_link")
    rospy.loginfo("Reach the workplace...Waiting for perception module...")
    
    objectOnHand = False
    _head = HeadClient()
    _pc = PerceptionClient()
    while not rospy.is_shutdown():
      _pc.stow()
      
      # Pick process
      fail_ct = 0
      while not rospy.is_shutdown() and not objectOnHand:
        rospy.loginfo("Start perception process")
        _pc.scanScene()
        pair_list, success = _pc.getGraspableObject()
        if not success:
          rospy.logwarn("Perception failed...")
          _pc.stow()
          head_client.look(0.9, 0.0, 0.5, "base_link")
          break
        
        pick_obj = pair_list[0].object
        grasps = pair_list[0].grasps
        print("Picking Target =====> " + pick_obj.name)
        if _pc.pick(pick_obj, grasps):
          objectOnHand = True
          break
        rospy.logwarn("Picking failed...")
        # Failed to pick objects, and retry
        _pc.stow()
        if fail_ct > 3:
          fail_ct = 0
          break
        fail_ct += 1

      # Place process
      place_fails = 0
      foundPlaceLocation = False
      while not rospy.is_shutdown() and objectOnHand:
          rospy.loginfo("Prepare to place object...")
          place_location = pair_list[1]
          print('Target location =====> ' + place_location.name)
          location_pose = PoseStamped()
          location_pose.pose = place_location.primitive_poses[0]
          location_pose.pose.position.z += 0.1
          location_pose.pose.position.x -= 0.05
            
          location_pose.header.frame_id = pick_obj.header.frame_id
          if _pc.place(pick_obj, location_pose):
            objectOnHand = False
            break
          rospy.logwarn("Placing Failed")
          # Failed to place objects into the target location, 
          # and place back to the origin position
          origin_pose = PoseStamped()
          origin_pose.pose = pick_obj.primitive_poses[0]
          origin_pose.pose.position.z += 0.03
          origin_pose.pose.position.z -= 0.02
          origin_pose.header.frame_id = pick_obj.header.frame_id
          if _pc.place(pick_obj, origin_pose):
            objectOnHand = False
            rospy.loginfo("Place object back to origin place...")
            break
      
      _pc.intermediate_stow()
      _pc.stow()
      rospy.loginfo("Finished...")
      # Debug
      if attemps > 50:
        rospy.signal_shutdown("Process Finished")