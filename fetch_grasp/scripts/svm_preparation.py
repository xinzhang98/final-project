#!/usr/bin/env python

import copy
import actionlib
import pickle
import rospy
import numpy as np
from cv_bridge import CvBridge
import cv2
import copy
import time
import math
from sklearn.cluster import KMeans
from math import sin, cos
from moveit_python import (MoveGroupInterface,
                           PlanningSceneInterface,
                           PickPlaceInterface)
from moveit_python.geometry import rotate_pose_msg_by_euler_angles

from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from control_msgs.msg import PointHeadAction, PointHeadGoal
from fetch_msgs.msg import DetectGraspableObjectAction, DetectGraspableObjectGoal
from geometry_msgs.msg import PoseStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from moveit_msgs.msg import PlaceLocation, MoveItErrorCodes
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import Image
from std_msgs.msg import String, Header
from cv_bridge import CvBridge, CvBridgeError
from gazebo_msgs.srv import (GetModelState, GetPhysicsProperties,
                             SetPhysicsProperties, SetModelState,
                             SetModelStateRequest, SpawnModel,
                             DeleteModel)
from geometry_msgs.msg import Pose, Point, Quaternion
from tf.transformations import quaternion_about_axis
import sklearn
from sklearn.preprocessing import LabelEncoder
import pickle

class Trainer(object):
    def __init__(self):
        find_topic = "/find_objects"
        rospy.loginfo("Waiting for %s..." % find_topic)
        self.find_client = actionlib.SimpleActionClient(
            find_topic, DetectGraspableObjectAction)
        self.find_client.wait_for_server()
        self.bridge = CvBridge()
        self.point2d = []
        self.cv_image = None
        self.sub_ = rospy.Subscriber("/head_camera/rgb/image_raw", Image, self.callback)
        # self.roi_list = []
        self.label_n_features = []
        self.models = ['demo_cube',
                       'coke_can',
                       'green_cube',
                       'red_cube',
                       'green_target',
                       'blue_target',
                       'beer']
        self.get_physics_properties_server = 'gazebo/get_physics_properties'
        self.set_physics_properties_server = 'gazebo/set_physics_properties'
        self.get_model_state_server = 'gazebo/get_model_state'
        self.set_model_state_server = 'gazebo/set_model_state'
        self.gazebo_spawn_server = 'gazebo/spawn_sdf_model'
        self.delete_model_server = 'gazebo/delete_model'
        self.delete_model_client = rospy.ServiceProxy(self.delete_model_server, DeleteModel)
        rospy.wait_for_service(self.delete_model_server)
        self.get_model_state_client = rospy.ServiceProxy(self.get_model_state_server, GetModelState)
        rospy.wait_for_service(self.get_model_state_server)
        self.set_model_state_client = rospy.ServiceProxy(self.set_model_state_server, SetModelState)
        rospy.wait_for_service(self.set_model_state_server)
        self.spawn_model_client = rospy.ServiceProxy(self.gazebo_spawn_server, SpawnModel)
        rospy.wait_for_service(self.gazebo_spawn_server)
        
        
        # Test Part
        self.model = pickle.load(open('/home/shin/fetch_ws/src/fetch_grasp/scripts/colourHistogram.sav', 'rb'))
        rospy.loginfo("Opened File")
        self.clf = self.model['classifier']
        self.encoder = LabelEncoder()
        self.encoder.classes_ = self.model['classes']
        self.scaler = self.model['scaler']
        rospy.loginfo("Loaded, Prepare to objct perception pipeline")
        
    def visualize_colors(self, cluster, centroids):
      # Get the number of different clusters, create histogram, and normalize
      labels = np.arange(0, len(np.unique(cluster.labels_)) + 1)
      (hist, _) = np.histogram(cluster.labels_, bins = labels)
      hist = hist.astype("float")
      hist /= hist.sum()

      # Create frequency rect and iterate through each cluster's color and percentage
      rect = np.zeros((50, 300, 3), dtype=np.uint8)
      colors = sorted([(percent, color) for (percent, color) in zip(hist, centroids)])
      start = 0
      for (percent, color) in colors:
          print(color, "{:0.2f}%".format(percent * 100))
          end = start + (percent * 300)
          cv2.rectangle(rect, (int(start), 0), (int(end), 50), \
                        color.astype("uint8").tolist(), -1)
          start = end
      return rect
    
    def compute_colour_histogram(self, image):
      bgr_planes = cv2.split(image)
      histSize = 256
      histRange = (0, 256)
      accumulate = False
      b_hist = cv2.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)
      g_hist = cv2.calcHist(bgr_planes, [1], None, [histSize], histRange, accumulate=accumulate)
      r_hist = cv2.calcHist(bgr_planes, [2], None, [histSize], histRange, accumulate=accumulate)
      hist_w = 512
      hist_h = 400
      bin_w = int(round(hist_w/histSize))
      histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)
      cv2.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      cv2.normalize(g_hist, g_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      cv2.normalize(r_hist, r_hist, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
      for i in range(1, histSize):
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(b_hist[i-1]) ),
              ( bin_w*(i), hist_h - int(b_hist[i]) ),
              ( 255, 0, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(g_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(g_hist[i]) ),
                ( 0, 255, 0), thickness=2)
        cv2.line(histImage, ( bin_w*(i-1), hist_h - int(r_hist[i-1]) ),
                ( bin_w*(i), hist_h - int(r_hist[i]) ),
                ( 0, 0, 255), thickness=2)
      return histImage
    
    def callback(self, data):
      try:
        self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      except CvBridgeError as e:
        print(e)

    def updateScene(self, label):
        goal = DetectGraspableObjectGoal()
        goal.plan_grasps = True
        self.find_client.send_goal(goal)
        self.find_client.wait_for_result(rospy.Duration(5.0))
        find_result = self.find_client.get_result()

        labels = []
        
        objects = list()
        idx = -1
        # rospy.loginfo('objects='+str(len(find_result.objects)))
        self.point2d = []
        self.roi_list = []
        fpfh = None
        feature = None
        for obj in find_result.objects:
            list_ = np.array(obj.object.project_points.data, np.float32)
            self.point2d.append(np.reshape(list_, (-1,2)))
            # FPFH Histogram
            fpfh_arr = np.array(obj.object.fpfh_descriptor.data, np.float32)
            # rospy.loginfo("histImage is " + str(obj.object.fpfh_descriptor.layout.dim[0]) +". FPFH is " + str(fpfh.shape))
            fpfh = fpfh_arr.reshape(obj.object.fpfh_descriptor.layout.dim[0].size, 33)
            fpfh = fpfh[:200]
        
        for point in self.point2d:
          #  Bounding Rect
          x,y,w,h=cv2.boundingRect(point)
          if w < 3 or h < 3:
            continue
          # cv2.rectangle(self.cv_image,(x,y),(x+w,y+h),(255,0,0),2)
          image = self.cv_image[y:y+h, x:x+w]
          
          # Colour Histogram
          histImage = self.compute_colour_histogram(image)
          # rospy.loginfo("histImage is " + str(histImage.shape) +". FPFH is " + str(fpfh.shape))
          feature = np.append(histImage[20:390], fpfh).astype(np.float64)
          rospy.loginfo("Feature size = " + str(feature.size))
          feature = histImage.reshape(400, 512*3).astype(np.float64)
          prediction = self.clf.predict(self.scaler.transform(feature.reshape(1,-1)))
          label_ = self.encoder.inverse_transform(prediction)[0]
          labels.append(label_)
          feature = None

        self.label_n_features.append([feature, str(label)])
        for l in labels:
          rospy.loginfo("Found a object named " + str(l))

    def gazebo_spawn_model(self, model_name, position, orientation):
      model_sdf_path = '/home/shin/fetch_ws/src/fetch_custom_gazebo/models/'+model_name+'/model.sdf'
      model_xml = ''
      with open(model_sdf_path, 'r') as xml_file:
        model_xml = xml_file.read().replace('\n','')

      model_pose = Pose()
      model_pose.position.x = position.x
      model_pose.position.y = position.y
      model_pose.position.z = position.z
      model_pose.orientation.x = orientation.x
      model_pose.orientation.y = orientation.y
      model_pose.orientation.z = orientation.z
      model_pose.orientation.w = orientation.w

      self.spawn_model_client(
        model_name = model_name,
        model_xml = model_xml,
        robot_namespace = '',
        initial_pose = model_pose,
        reference_frame = 'world'
      )
    
    def remove_model(self, model_name):
      self.delete_model_client(model_name)
  
    def run(self):
      for model in self.models:
        z = 0
        x = 0
        total = 0
        step = 0
        index = 0
        if 'cube' in str(model):
          x = 0.5
          z = 0.714093
          total = 57
          step = 7
        if 'coke_can' in str(model):
          x = 0.5
          z = 0.682308
          total = 181
          step = 30
        if 'beer' in str(model):
          x = 0.51
          z = 0.685001
          total = 181
          step = 30
        if 'target' in str(model):
          x = 0.567525
          z = 0.678008
          total = 361
          step = 90

        while x < 1.1:
          position = Point(x = x, y = 0.0, z = z)
          for angle in range(0, total, step):
            rospy.loginfo("x = " + str(x) +" angle = " + str(angle))
            theta = math.radians(angle)
            q = quaternion_about_axis(theta, (0,0,1))
            orientation = Quaternion(*q)
            image = self.gazebo_spawn_model(str(model), position, orientation)
            rospy.sleep(1)
            self.updateScene(str(model))
            self.remove_model(str(model))
            rospy.sleep(0.5)
          index += 1
          x += 0.2
      pickle.dump(self.label_n_features, open('/home/shin/fetch_ws/src/fetch_grasp/data/training_set4.sav', 'wb'))
      rospy.signal_shutdown("process is down")

if __name__ == "__main__":
    # Create a node
    rospy.init_node("svm_preparation_node")
    
    rospy.loginfo("Starting preparation process...")
    
    while not rospy.Time.now():
        pass
    # Setup clients
    trainer_ = Trainer()
    
    # labeled_features = []
    while not rospy.is_shutdown():
      rospy.sleep(1)
      # trainer_.run()
      trainer_.updateScene("label")
      
      # trainer_ = Trainer()

