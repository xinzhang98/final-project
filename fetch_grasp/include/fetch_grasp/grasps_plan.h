#ifndef GRASPS_PLAN_H
#define GRASPS_PLAN_H

#include <ros/ros.h>
#include <fetch_msgs/GraspableObject.h>

namespace fetch_grasp {
  class GraspsPlanner
{
public:
  /**
   * @brief Constructor, loads grasp planner configuration from ROS params.
   * @param nh Nodehandle to use for accessing grasp planner parameters.
   */
  GraspsPlanner(ros::NodeHandle& nh);
  int plan(const fetch_msgs::Object& object,
            std::vector<moveit_msgs::Grasp>& grasps);

private:
  /**
   *  @brief Generate a grasp, add it to internal grasps_
   *  @param pose The pose of the end effector tool point
   *  @param gripper_pitch The pitch of the gripper on approach
   *  @param x_offset The offset in the x direction (in).
   *  @param z_offset The offset in the z direction (up).
   *  @param quality The quality to ascribe to this grasp.
   *  @returns The number of grasps generated.
   */
  int createGrasp(const geometry_msgs::PoseStamped& pose,
                  double gripper_opening,
                  double gripper_pitch,
                  double x_offset,
                  double z_offset,
                  double quality);

  /**
   *  @brief Generate a series of grasps around the edge of a shape
   *  @param pose The pose of the end effector tool point.
   *  @param depth The depth of the shape.
   *  @param width The width of the shape.
   *  @param height The height of the shape.
   *  @param use_vertical Whether to include vertical poses. If coming
   *         from two sides, the second call probably should not generate
   *         vertical poses.
   *  @returns The number of grasps generated.
   */
  int createGraspSeries(const geometry_msgs::PoseStamped& pose,
                        double depth, double width, double height,
                        bool use_vertical = true);


  trajectory_msgs::JointTrajectory makeGraspPosture(double pose);

  // gripper model
  std::string left_joint_, right_joint_;
  double max_opening_;
  double max_effort_;
  double grasp_duration_;
  double tool_offset_;
  double finger_depth_;
  double gripper_tolerance_;

  // approach model
  std::string approach_frame_;
  double approach_min_translation_;
  double approach_desired_translation_;

  // retreat model
  std::string retreat_frame_;
  double retreat_min_translation_;
  double retreat_desired_translation_;

  // storing of internal grasps as we generate them
  std::vector<moveit_msgs::Grasp> grasps_;
};

}

#endif