#ifndef FETCH_GRASP_OBJECT_SEGMENTATION_H
#define FETCH_GRASP_OBJECT_SEGMENTATION_H

#include <ros/ros.h>
#include <vector>
#include <fetch_msgs/Object.h>
#include <pcl/io/io.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>
#include <pcl/PCLPointCloud2.h>
#include <boost/foreach.hpp>
#include <image_geometry/pinhole_camera_model.h>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
using namespace std;
// using namespace pcl;

namespace fetch_grasp {

	class ObjectSegmentation {
		public:

			ObjectSegmentation(ros::NodeHandle &nh);

			void segment(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud,
						 vector<fetch_msgs::Object> &objects,
						 vector<fetch_msgs::Object> &planes,
						 pcl::PointCloud<pcl::PointXYZRGB> &objects_cloud,
						 pcl::PointCloud<pcl::PointXYZRGB> &planes_cloud);
      
      // void compute_fpfh_n_project_points(const pcl::PointCloud<pcl::PointXYZRGB> &cloud, std_msgs::Float32MultiArray &output);
      void pc2img(const pcl::PointCloud<pcl::PointXYZRGB> &data);
      // void imageCb(const sensor_msgs::ImageConstPtr& image_msg,
      //              const sensor_msgs::CameraInfoConstPtr& info_msg);
      void subCb(const sensor_msgs::CameraInfoConstPtr& info_msg);

		private:
			pcl::VoxelGrid<pcl::PointXYZRGB> vg;
			pcl::SACSegmentation<pcl::PointXYZRGB> seg;
			pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> extract_cluster;
			pcl::ExtractIndices<pcl::PointXYZRGB> extract_indices;

      // image_transport::ImageTransport it_;
      // image_transport::CameraSubscriber img_sub;
			// ros::NodeHandle nh;
      ros::Subscriber sub_;
      vector<cv::Point3f> pointsFromPc;
      vector<cv::Point2f> points2Img;
      image_geometry::PinholeCameraModel cam_model_;
      tf::TransformListener tf_listener_;
	};
}

#endif