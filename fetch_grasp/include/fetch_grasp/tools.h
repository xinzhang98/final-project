#ifndef FETCH_GRASP_TOOLS_H
#define FETCH_GRASP_TOOLS_H

#include <Eigen/Eigen>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <vector>
#include <math.h>
#include <Eigen/Eigen>
#include <pcl/filters/project_inliers.h>
#include <pcl/surface/convex_hull.h>
#include <shape_msgs/SolidPrimitive.h>
#include <geometry_msgs/Pose.h>

/*
   The code based on online resources
*/

namespace fetch_grasp {
  // Reference: https://github.com/PointCloudLibrary/pcl/blob/master/sample_consensus/include/pcl/sample_consensus/sac_model_plane.h
  double distancePointToPlane(const Eigen::Vector4f& point, const pcl::ModelCoefficients::Ptr plane);


  // Reference: https://gis.stackexchange.com/questions/22895/finding-minimum-area-rectangle-for-given-points
  bool extractObjectBoundingBox(const pcl::PointCloud<pcl::PointXYZRGB>& input,
                                pcl::PointCloud<pcl::PointXYZRGB>& output,
                                shape_msgs::SolidPrimitive& shape,
                                geometry_msgs::Pose& pose);

  bool extractPlaneBoundingBox(const pcl::PointCloud<pcl::PointXYZRGB>& input,
                                  shape_msgs::SolidPrimitive& shape,
                                  geometry_msgs::Pose& pose);
}

#endif
