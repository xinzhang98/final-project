# An automatic grasp system for manipulator robot

INTRODUCTION
------------

And automatic grasp system built for the Fetch mobile robot.
This project focuses more on object recognition and grasping at close range.



REQUIREMENTS
------------

This module requires:

 * Ubuntu 18.04.5 LTS  (https://releases.ubuntu.com/18.04/)
 * ROS Melodic Morenia (https://wiki.ros.org/melodic)
 * Gazebo 9            (http://gazebosim.org/)
 * MoveIt              (https://moveit.ros.org/)

 CONFIGURATION
-------------
 Git clone this project and do the following in the directory:
 * catkin_make
 * source devel/setup.bash
 * roslaunch fetch_custom_gazebo simple_grasp.launch
 * roslaunch fetch_moveit_config fetch_planning_execution.launch
 * roslaunch fetch_grasp perception_launch.launch
 * rosrun fetch_grasp perception_client.py

 

